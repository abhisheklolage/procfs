#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<stdint.h>

typedef struct stat_cpu_time{
    char cpu_name[5];
    uint32_t t_user;
    uint32_t t_niced_user;
    uint32_t t_kernel;
    uint32_t t_idle;
    uint32_t t_iowait;
    uint32_t t_irq;
    uint32_t t_softirq;
}stat_cpu_time;

void getSomeFile(){
    FILE* proc_fd;
    char byte;

    proc_fd = fopen("/proc/cpuinfo", "r");
    if(proc_fd == NULL){
        printf("Could not read file.\n");
        exit(1);
    }
    else{
        while(fread(&byte, sizeof(byte), 1, proc_fd) == 1){
                printf("%c", byte);
        }
    }
    fclose(proc_fd);
}

size_t getNumberOfProcessors(){
    FILE* cpuinfo_fd;
    char* line = NULL;
    size_t len;
    ssize_t read;
    size_t number_of_cpus = 0;

    cpuinfo_fd = fopen("/proc/cpuinfo", "r");
    while((read = getline(&line, &len, cpuinfo_fd)) != -1){
        if(line[0] == '\n'){
            // one processor entry checked
            number_of_cpus += 1;
        }
    }
    if(line){
         free(line);
    }
    return number_of_cpus;
}

void getTimeStats(uint32_t sample_rate, uint32_t emit_rate){
    /*
     *The percentage of time the processor(s) spend in user mode, system mode,
     and the percentage of time *the processor(s) are idle
     */
    printf("CPU Time Statistics:\n");
    FILE* stat_fd;
    char* line = NULL;
    ssize_t read = 0;
    size_t len = 0;
    size_t number_of_processors = getNumberOfProcessors();
    size_t number_of_lines = number_of_processors + 1; // extra line for total
    size_t line_idx = 0;
    char* cpu_info[number_of_lines];
    size_t idx = 0;

    uint32_t emit_freq = emit_rate / sample_rate;
    uint32_t time_slept = 0;
    stat_cpu_time cpu_time;
    stat_cpu_time avg_cpu_time;

    for(idx = 0; idx < number_of_processors; idx++){
        cpu_info[idx] = NULL;
    }

    printf("Number of processors: %zd\n", number_of_processors);

    // initialize struct for average

    while(1){
        line_idx = 0;
        // if(time is equal to sample time, do sampling)
        stat_fd = fopen("/proc/stat", "r");
        if(stat_fd == NULL){
            printf("Could not read file.\n");
            exit(1);
        }
        else{
            // get number of cpus on the system from /proc/cpuinfo
            while((line_idx < number_of_lines) && ((read = getline(&line, &len, stat_fd)) != -1)){
                //printf("%s", line);
                cpu_info[line_idx] = (char *) malloc(read * sizeof(char));
                strcpy(cpu_info[line_idx], line);
                line_idx += 1;
            }
        }
        fclose(stat_fd);

        line_idx = 0;
        char cpu_info_raw[11][128];
        char* splitter;
        while(line_idx < number_of_lines){
            //printf("%s", cpu_info[line_idx]);
            // parse the line

            splitter = strtok(cpu_info[line_idx], " ");
            idx = 0;
            while(splitter != NULL){
                strcpy(cpu_info_raw[idx++], splitter);
                splitter = strtok(NULL, " ");
            }
            // put values into struct
            strcpy(cpu_time.cpu_name, cpu_info_raw[0]);
            cpu_time.t_user = atoi(cpu_info_raw[1]);
            cpu_time.t_kernel = atoi(cpu_info_raw[3]);
            cpu_time.t_idle = atoi(cpu_info_raw[4]);
            // parsing complete
            //printf("%s---%d---%d----%d\n", cpu_time.cpu_name, cpu_time.t_user, cpu_time.t_kernel, cpu_time.t_idle);
            line_idx++;
        }

        if((emit_rate - time_slept) <= (emit_rate % sample_rate)){
            // sleep for remainder time
            //printf("Sleeping for %d seconds\n", emit_rate - time_slept);

            //avg_cpu_time.t_user += cpu_time.t_user;
            //avg_cpu_time.t_kernel += cpu_time.t_kernel;
            //avg_cpu_time.t_idle += cpu_time.t_idle;

            sleep(emit_rate - time_slept);
            time_slept += emit_rate - time_slept;
            //printf("Time slept is %d seconds\n", time_slept);
            // print average
            avg_cpu_time.t_user /= emit_rate;
            avg_cpu_time.t_kernel /= emit_rate;
            avg_cpu_time.t_idle /= emit_rate;
            printf("Averages:\n");
            printf("%s---%d---%d----%d\n\n", "avg_cpu_stat", avg_cpu_time.t_user, avg_cpu_time.t_kernel, avg_cpu_time.t_idle);
            time_slept = 0;
            avg_cpu_time.t_user = 0;
            avg_cpu_time.t_kernel = 0;
            avg_cpu_time.t_idle = 0;
        }
        else{
            avg_cpu_time.t_user += cpu_time.t_user;
            avg_cpu_time.t_kernel += cpu_time.t_kernel;
            avg_cpu_time.t_idle += cpu_time.t_idle;

            printf("Sum:\n");
            printf("%s---%d---%d----%d\n\n", "avg_cpu_stat", avg_cpu_time.t_user, avg_cpu_time.t_kernel, avg_cpu_time.t_idle);


            sleep(sample_rate);
            time_slept += sample_rate;
            //printf("Sleeping for %d seconds\n", sample_rate);
            //printf("Time slept is %d seconds\n", time_slept);
        }

        // free the last read lines
        //if(line){
        //     free(line);
        //     line == NULL;
        //}
        for(idx = 0; idx < number_of_lines; idx++){
            free(cpu_info[idx]);
        }
    }

}

int main(int argc, char* const argv[]){

    printf("%d\n", argc);
    if(argc != 1 && argc != 3){
        printf("Format ./proc_parse <read_rate> <print_rate> OR ./proc_parse");
        exit(1);
    }

    if(argc == 1){
        // print default stats
        printf("No extra arguments\n");
    }

    else{
        printf("Two extra arguments\n");
        // sample, average and print
        uint32_t sample_rate = atoi(argv[1]);
        uint32_t emit_rate = atoi(argv[2]);
        getTimeStats(sample_rate, emit_rate);
    }
}
